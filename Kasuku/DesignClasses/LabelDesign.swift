import UIKit

/**
 *  UILabel Custom Class for LabelDesign.
 *
 *  @superclass UILabel
 */

class LabelDesign: UILabel {
   
    //MARK:- Life Cycle Method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Adjusting UIFont type according to the Tag provided over UIStoryboard.
        
            self.font = UIFont(name: Constant.ElikaFont.regular, size: self.font.pointSize)
       
       
    }
}
