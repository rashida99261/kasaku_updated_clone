import UIKit

/**
 *  Extension to provide extra capabilities to existing UIColor.
 */

extension UIColor {
     //MARK:- Properties
    var redValue: CGFloat{ return CIColor(color: self).red }
    var greenValue: CGFloat{ return CIColor(color: self).green }
    var blueValue: CGFloat{ return CIColor(color: self).blue }
    var alphaValue: CGFloat{ return CIColor(color: self).alpha }
}

/**
 *  Extension to provide extra capabilities to existing UIColor.
 */

extension UIColor{
    
    //MARK:- Methods
    /**
     *  Method for getting Elika Primary Color.
     *
     * @return UIColor
     */
    class func elikaPrimaryColor() -> UIColor {
        return UIColor.black
    }
    
    class func textfieldPaddingColor()-> UIColor{
        return UIColor(red: 137/255, green: 137/255, blue: 137/255, alpha: 1.00)
    }
    
    

    class func appThemeColor() -> UIColor {
         return UIColor(red: 01/255, green: 165/255, blue: 226/255, alpha: 1.00)
     
    }
    
    /**
     *  Method for getting Elika Hairline Color.
     *
     * @return UIColor
     */
    class func hairline() -> UIColor {
       return UIColor(red: 0.812, green: 0.835, blue: 0.859, alpha: 1.00)
    }

    /**
     *  Method for getting Elika Custom Grey Color.
     *
     * @return UIColor
     */
    class func elikaGreyColor() -> UIColor{
        return UIColor(red: 0.573, green: 0.655, blue: 0.718, alpha: 1.00)
    }
    
    /**
     *  Method for getting Elika Activity Background Color.
     *
     * @return UIColor
     */
    class func elikaActivityBackground() -> UIColor{
        return UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
    }
    
    /**
     *  Method for getting Elika Yellowish Color.
     *
     * @return UIColor
     */
    class func elikaYellowishColor() -> UIColor{
        return UIColor(red: 1.000, green: 0.753, blue: 0.071, alpha: 1.000)
    }
    
    /**
     *  Method for getting Elika Custom Camera Grey Color.
     *
     * @return UIColor
     */
    class func elikaCameraGrey()-> UIColor{
        return UIColor(red: 0.467, green: 0.549, blue: 0.624, alpha: 1.00)
    }
    
    /**
     *  Method for getting Elika Custom Text Padding Color.
     *
     * @return UIColor
     */
    class func elikaTextPadding()->UIColor{
        return UIColor(red: 0.725, green: 0.753, blue: 0.784, alpha: 1.00)
    }
    
    /**
     *  Method for getting Elika Custom Usage Header Background Color.
     *
     * @return UIColor
     */
    class func usageHeaderBackground() -> UIColor{
        return UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
    }
    
    /**
     *  Method for getting Elika Custom Unselected Menu Item Background Color.
     *
     * @return UIColor
     */
    class func unselectedMenuItem() -> UIColor{
        return UIColor(red: 0.424, green: 0.459, blue: 0.486, alpha: 1.00)
    }
    
    /**
     *  Method for getting Elika Custom Call Button Border Color.
     *
     * @return UIColor
     */
    class func callButtonBorder() -> UIColor{
        return UIColor(red: 0.929, green: 0.937, blue: 0.949, alpha: 1.00)
    }
    
    /**
     *  Method for getting Elika Custom Unselected Access Types Color.
     *
     * @return UIColor
     */
    class func navigationBarColor() -> UIColor{
        return UIColor(red: 37/255, green: 38/255, blue: 37/255, alpha: 1.00)
    }
    
    /**
     *  Method for getting Elika Custom Loader Background Color.
     *
     * @return UIColor
     */
    class func loaderBackground() -> UIColor{
        return UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.4)
    }
    
    /**
     *  Method for getting Elika Custom Greyish Background Color for Admin WIFI Configuration.
     *
     * @return UIColor
     */
    class func adminCustomGreyish() -> UIColor{
        return UIColor(red: 233/255, green: 231/255, blue: 231/255, alpha: 1.0)
    }
    
}
extension UIColor {
    
    // MARK: - Initialization
    
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt32 = 0
        
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        let length = hexSanitized.characters.count
        
        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }
        
        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0
            
        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0
            
        } else {
            return nil
        }
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
    
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to String
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
    
    
}
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
