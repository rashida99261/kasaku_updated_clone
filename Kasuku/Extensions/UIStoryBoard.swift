import UIKit

/**
 *  Extension to provide extra capabilities to existing UIStoryboard.
 */

extension UIStoryboard{
    
    /**
     *  Method for instantiateViewController.
     *
     * @param T UIViewController
     * @return T UIViewController
     */
    func instantiateViewController<T:UIViewController>(type: T.Type) -> T? {
        var fullName: String = NSStringFromClass(T.self)
        if let range = fullName.range(of:".", options:.backwards, range:nil, locale: nil){
            fullName = fullName.substring(from: range.upperBound)
        }
        return self.instantiateViewController(withIdentifier:fullName) as? T
    }
}
