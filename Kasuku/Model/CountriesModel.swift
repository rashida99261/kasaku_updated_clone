import Foundation
import SwiftyJSON

class CountriesModel: NSObject {
    var name : String?
    var code: String?
    
    init(json: JSON) {
        name = json["name"].stringValue
        code = json["code"].stringValue
    }
}
