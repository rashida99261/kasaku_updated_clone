import Foundation

class NoteModel: NSObject {
    var id: String? = ""
    var address: String? = ""
    var alarm: String? = ""
    var archived: Bool = false
    var attachments = [AttachmentModel]()
    var checklist: Bool = false
    var content: String? = ""
    var creation: Int64 = 0
    var last_modification: Int64 = 0
    var group_id: String? = nil
    var latitude:NSNumber = 0
    var locked:Bool = false
    var longitude:NSNumber = 0
    var owner:String = ""
    var recurrence_rule:String? = nil
    var reminder_fired: Bool = false
    var title: String? = ""
    var trashed: Bool = false
    var category_id: String? = ""
    var type: String? = "note"
    var dateCreate: String? = ""
    var userId: String? = ""
    var color_note : String? = ""
 //   var notebook_id: String? = ""

    
    override init() {
        super.init()
    }
    
    init(
        id: String, address: String, alarm: String, archived: Bool = false,
        checklist: Bool, content: String, creation: Int64, last_modification: Int64, group_id: String, latitude:NSNumber, locked:Bool, longitude:NSNumber, owner:String, recurrence_rule:String, reminder_fired: Bool, title: String, trashed: Bool, category_id: String , dateCreate: String, userId: String, color_note: String) {
        self.id = id
        self.address = address
        self.alarm = alarm
        self.archived = archived
        self.checklist = checklist
        self.content = content
        self.creation = creation
        self.last_modification = last_modification
        self.group_id = group_id
        self.latitude = latitude
        self.locked = locked
        self.longitude = longitude
        self.owner = owner
        self.recurrence_rule = recurrence_rule
        self.reminder_fired = reminder_fired
        self.title = title
        self.trashed = trashed
        self.category_id = category_id
        self.dateCreate = dateCreate
        self.userId = userId
        self.color_note = color_note
       // self.notebook_id = notebook_id
        
    }
    
    
    
}
