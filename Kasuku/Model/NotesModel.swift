import UIKit

class NotesModel: NSObject {

}

enum NotebookType: String {
    case note = "note"
    case image = "image"
    case handwriting = "handwriting"
    case vidio = "vidio"



}

struct NotesData {
    var title :  String?
    var id :  String?
    var des :  String?
    var isReminder : Bool?
    var alarm :  String?
    var noteBook :  NotebookData?
    var type :  NotebookType?
    var imagePath :  String?
    var creationDate: String?




}
struct NotebookData {
    var titleNotebook :  String?
    var id :  String?
    var color :  String?
}
