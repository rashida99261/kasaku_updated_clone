import Foundation

class SignUpModel: NSObject {
    var firstName: String? = ""
    var lastName: String? = ""
    var mobileNumber: String? = ""
    var emailID: String? = ""
    var password: String? = ""
    var confirmPassword: String? = ""
    
    override init() {
        super.init()
    }
}
