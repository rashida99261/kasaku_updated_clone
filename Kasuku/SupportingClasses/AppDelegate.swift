import UIKit
import CoreData
import IQKeyboardManagerSwift
import REFrostedViewController
import GoogleSignIn
import FBSDKLoginKit
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    class func sharedInstance() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GIDSignIn.sharedInstance().clientID = "747363308629-1lo0u4ateblo1e11q4130353h3fts2l9.apps.googleusercontent.com"
        //747363308629-o4hf2qc9i8d01rh0p65mop55ig09nfto.apps.googleusercontent.com
        GIDSignIn.sharedInstance().signOut()
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
         self.logoutFacebook()
        
        
        self.window?.tintColor = UIColor.appThemeColor()
        
        // Configure IQKeyboardManager
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.placeholderFont = UIFont(name: Constant.ElikaFont.regular, size: 16)
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        return true
    }
    func setRoot(name: String, email : String, createdAt: Int, profilePic: String){
        if(!UserDefaults.standard.bool(forKey: Constant.DefaultStorage.IS_LOGGED_IN)){
            UserDefaults.standard.set(true, forKey: Constant.DefaultStorage.IS_LOGGED_IN)
            UserDefaults.standard.set(name, forKey: Constant.DefaultStorage.USER_NAME)
            UserDefaults.standard.set(email, forKey: Constant.DefaultStorage.USER_EMAIL)
            UserDefaults.standard.set(createdAt, forKey: Constant.DefaultStorage.USER_CREATION)
            UserDefaults.standard.set(profilePic, forKey: Constant.DefaultStorage.USER_PROFILE)
            UserDefaults.standard.synchronize()
        }
        Shared.sharedInstance.userName = name
        Shared.sharedInstance.userEmail = email
        Shared.sharedInstance.imageUrl = profilePic
        AppDelegate.sharedInstance().window = UIWindow(frame: UIScreen.main.bounds)
        let sB = UIStoryboard(name: "Main", bundle: nil)
        let centerCont = sB.instantiateViewController(withIdentifier: "homenav")
        let leftCont = sB.instantiateViewController(type: LeftMenuVC.self)!
        let container = REFrostedViewController(contentViewController: centerCont, menuViewController: leftCont)
        let navCont = UINavigationController(rootViewController: container!)
        if Constant.DefaultStorage.isFromLeftMenu == "1"{
            Constant.DefaultStorage.isFromLeftMenu = ""
            navCont.setNavigationBarHidden(true, animated: false)
        }else{
            navCont.setNavigationBarHidden(true, animated: true)
        }

        AppDelegate.sharedInstance().window?.rootViewController = navCont
        AppDelegate.sharedInstance().window?.tintColor = UIColor.appThemeColor()
        AppDelegate.sharedInstance().window?.makeKeyAndVisible()
    }
    
    
    func logoutFacebook(){
        if ((AccessToken.current) != nil) {
            print("user is logged in")
            let fbLoginManager : LoginManager = LoginManager()
            fbLoginManager.logOut()
        }else{
            print("user is not logged in")
        }
    }
    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return ApplicationDelegate.shared.application(app, open: url, options: options) || (GIDSignIn.sharedInstance()?.handle(url)) ?? false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    @available(iOS 10.2, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Kasuku")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.2, *) {
            let context = persistentContainer.viewContext
        } else {
            // Fallback on earlier versions
        }
       /* if connect{
            do {
                try connect.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }*/
    }

}

