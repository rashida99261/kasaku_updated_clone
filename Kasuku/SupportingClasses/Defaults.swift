import UIKit

class Defaults: NSObject {
    
    static let sharedPref = Defaults()
    
    let defaults = UserDefaults.standard
    
    
    func setLoggedInStatus(status : Bool){
        defaults.set(status, forKey: "kLoginStatus")
    }
    
    func getLoggedInStatus() -> Bool {
        return defaults.bool(forKey: "kLoginStatus")
    }
    
    
    func saveUserInfo(object : UserInfo){
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        defaults.set(data, forKey: "userInfo")
    }
    
    func retrieveUserInfo() -> UserInfo?{
        if let user = defaults.object(forKey: "userInfo") as? Data{
            let info =  NSKeyedUnarchiver.unarchiveObject(with: user)
            return info as? UserInfo
        }
        return nil
    }
    
    
    
    
}
