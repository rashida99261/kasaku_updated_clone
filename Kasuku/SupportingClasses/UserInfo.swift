import Foundation
import SwiftyJSON

class UserInfo : NSObject,NSCoding
{
    var userId : String?
    var firstName : String?
    var lastName : String?
    var email : String?
    var profileImage : String?
    
    

    override init() {
        
    }

    public init(json : JSON){
        userId = json["userId"].stringValue
        firstName = json["firstName"].stringValue
        lastName = json["lastName"].stringValue
        email = json["email"].stringValue
        profileImage = json["profileImage"].stringValue
       
    }

   required public init(coder aDecoder: NSCoder){
        self.userId = aDecoder.decodeObject(forKey: "userId") as? String
        self.firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        self.lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
    
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.userId, forKey: "userId")
        aCoder.encode(self.firstName, forKey: "firstName")
        aCoder.encode(self.lastName, forKey: "lastName")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.profileImage, forKey: "profileImage")
       
    }
    
}


