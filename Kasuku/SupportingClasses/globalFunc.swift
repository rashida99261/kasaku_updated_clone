//
//  globalFunc.swift
//  Kasuku
//
//  Created by Reinforce on 12/02/20.
//  Copyright © 2020 frestyle36. All rights reserved.
//

import Foundation
import UIKit

class Globalfunc {
    
    static func getHeaderViewHeightForCurrentDevice() -> CGFloat {
        switch UIScreen.main.nativeBounds.height {
        case 2436 , 1792 , 2688: // iPhone X,Xr,Xs,Xs max
            return 144
        default: // Every other iPhone
            return 64
        }
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

}

