import UIKit
import Navajo_Swift

class SignUpTableCell: UITableViewCell {
    
    @IBOutlet weak var lblFirstName: LoginTextField!
    @IBOutlet weak var lblLastName: LoginTextField!
    @IBOutlet weak var txtCountry: LoginTextField!
    @IBOutlet weak var txtMobileNumber: LoginTextField!
    @IBOutlet weak var txtEmail: LoginTextField!
    @IBOutlet weak var txtPassword: LoginTextField!
    @IBOutlet weak var txtConfirmPassword: LoginTextField!
    @IBOutlet weak var btnCheckBoxTerms: UIButton!
    
    var viewModel : SignUpViewModel!{
        didSet{
            if viewModel.termsAccepted{
                self.btnCheckBoxTerms.setTitle("v", for: .normal)
                self.btnCheckBoxTerms.setTitleColor(UIColor.appThemeColor(), for: .normal)
            }else{
                self.btnCheckBoxTerms.setTitle("u", for: .normal)
                self.btnCheckBoxTerms.setTitleColor(UIColor.lightGray, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setDelegate()
        var label = UILabel()
        txtCountry.rightViewMode = UITextFieldViewMode.always
        label = UILabel(frame: CGRect(x: 0, y: 0, width: 45, height: 26))
        label.font = UIFont(name: "elika-5th-oct", size: 23)
        label.textAlignment = .center
        label.textColor = UIColor.black
        label.text = "I"
        txtCountry.rightView = label
        
        let placeholderAttrs = [NSAttributedStringKey.foregroundColor : UIColor.black]
        let placeholder = NSAttributedString(string: txtCountry.placeholder!, attributes: placeholderAttrs)
        txtCountry.attributedPlaceholder = placeholder
        txtCountry.font = UIFont(name:Constant.ElikaFont.regular, size: 15)!
        
        
        // Initialization code
    }
    
    
    func setDelegate(){
        self.lblFirstName.delegate = self
        self.lblLastName.delegate = self
        self.txtCountry.delegate = self
        self.txtMobileNumber.delegate = self
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        self.txtConfirmPassword.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setBottomHairLine(textField: self.txtCountry)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setBottomHairLine(textField : UITextField){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: textField.frame.height + 10, width: Constant.ScreenDimension.width - 50, height: 1.0)
        bottomLine.backgroundColor = UIColor.appThemeColor().cgColor
        textField.borderStyle = UITextBorderStyle.none
        textField.layer.addSublayer(bottomLine)
    }
    
}
extension SignUpTableCell: UITextFieldDelegate{
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 101{
            if case Country.none = viewModel.countrySelected {
                return false
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCountry{
            let pickerView = UIPickerView()
            pickerView.delegate = self
            self.txtCountry.text = viewModel.titleForPicker(at: 0)
             self.selectLeftView(at : 0)
            viewModel.didSelect(at: 0)
            textField.inputView = pickerView
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == lblFirstName{
            viewModel.signUpDetails.firstName = textField.text
            return
        }
        
        if textField == lblLastName{
            viewModel.signUpDetails.lastName = textField.text
            return
        }
        
        if textField == txtMobileNumber{
            viewModel.signUpDetails.mobileNumber = textField.text
            return
        }
        
        if textField == txtEmail{
            viewModel.signUpDetails.emailID = textField.text
            return
        }
        
        if textField == txtPassword{
            viewModel.signUpDetails.password = textField.text
            return
        }
        
        if textField == txtConfirmPassword{
            viewModel.signUpDetails.confirmPassword = textField.text
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtPassword{
        let trimmedString = string.replacingOccurrences(of: " ", with: "")
        
        var textFieldText: NSString = ""
        
        if let text = self.txtPassword.text{
            textFieldText = text as NSString
        }
        
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: trimmedString)
    
        let strength = Navajo.strength(ofPassword: txtAfterUpdate)
        guard let _ = textField.rightView else {return true}
        self.txtPassword.setImage(text: Navajo.localizedString(forStrength: strength), color: UIColor.black)
        }
        return true
    }
}

extension SignUpTableCell:UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return viewModel.pickerComponent
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.titleForPicker(at: row)
    }
}

extension SignUpTableCell:UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return viewModel.pickerHeight
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtCountry.text = viewModel.titleForPicker(at: row)
        self.selectLeftView(at : row)
        viewModel.didSelect(at: row)
    }

    func selectLeftView(at row: Int){
        guard let _ = self.txtMobileNumber.leftView else { return }
        self.txtMobileNumber.setLeftImage(text: "+" + viewModel.codeForPicker(at: row), color: UIColor.black, leftTextfield: self.txtMobileNumber)
    }
}
