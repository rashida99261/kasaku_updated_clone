import UIKit
import JJFloatingActionButton
import SDWebImage

class HomeVC: BaseVC, UITableViewDelegate ,UITableViewDataSource, ComposeNotesDelegate, UITextFieldDelegate {
    func loadNotes(noteObj: NotesData) {
        self.notesList.append(noteObj)
        self.notelistTV.reloadData()
    }
    @IBOutlet weak var lblHeader: LabelDesign!
    
    var groupList = [GroupModel]()
    var strSearch:String=""

    @IBOutlet weak var notelistTV: UITableView!
    @IBOutlet weak var noteBtnCenter: UIButton!
    @IBOutlet weak var noNoteMsg: LabelDesign!
    @IBOutlet weak var noNoteDesc: LabelDesign!
    @IBOutlet weak var txtSearchTable: UITextField!

    var notes = [NoteModel]()
    var notesList = [NotesData]()
    var SearchData = [NoteModel]()
    @IBOutlet weak var btnFloat: UIView!
    let actionButton = JJFloatingActionButton()
    
    @IBOutlet weak var viewSearch: UIView!
    
    var jotVC = ExampleViewController()
    @IBOutlet weak var hgtHeader : NSLayoutConstraint!
    
    
    @objc func panGestureRecognized(sender : UIPanGestureRecognizer){
        
        self.frostedViewController.panGestureRecognized(sender)
    }
    
    func addSideGesture(sender : UIViewController){
        let panGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.panGestureRecognized(sender:)))
        panGesture.edges = .left
        sender.frostedViewController.limitMenuViewSize = true
        sender.frostedViewController.menuViewSize = CGSize(width:Constant.ScreenDimension.width * 0.78,height:Constant.ScreenDimension.height)
        sender.view.addGestureRecognizer(panGesture)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        actionButton.frame = self.btnFloat.frame
    }
    
    func getDataToDisplay(){
        
        
        notes = self.getNoteList()
        SearchData = notes
        
        if(notes.count == 0){   //|| self.notesList.count == 0
            notelistTV.isHidden = true
            return
        }
            
        else{
            notelistTV.isHidden = false
            notelistTV.reloadData()
        }
        
        //        print("Notes Count:",notes.count, notes[0].title)
        self.notelistTV.reloadData()
    }
    //TextFeild Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if string.isEmpty
        {
            strSearch = String(strSearch.dropLast())
        }
        else
        {
            strSearch=textField.text!+string
        }

        print(strSearch)
        //"SELF contains[c] %@"
        //"SELF.title CONTAINS[cd] %@"
        //let predicate=NSPredicate(format: "SELF.NoteModel.title CONTAINS[cd] %@", strSearch)
       // print(predicate)
       // let arr=(notes as NSArray).filtered(using: predicate)
//        let arr = notes.filter{ $0.title == obj?.title }
        let resultPredicate = NSPredicate(format: "self contains[cd] %@", strSearch)

        let arr = notes.filter {
            resultPredicate.evaluate(with: $0.title)
        }
        print(arr)
        if arr.count > 0
        {
            SearchData.removeAll(keepingCapacity: true)
            SearchData=arr
        }
        else
        {
            SearchData=notes
        }
        notelistTV.reloadData()
        return true
    }
    
    
    func getArchiveDataToDisplay(){
        notes = self.getArchiveNoteList()
        if(notes.count == 0){
            notelistTV.isHidden = true
            
            UserDefaults.standard.removeObject(forKey: "Archive")
            Shared.sharedInstance.selectedIndex = 0
            NotificationCenter.default.post(name: .didLoadNoteData, object: nil)
            
            return
        }
            
        else{
            
            SearchData=notes
            notelistTV.isHidden = false
            
        }
        
        //        print("Notes Count:",notes.count, notes[0].title)
        self.notelistTV.reloadData()
    }
    
//    func getArchiveTrashDataToDisplay(){
//
//        notes = self.getArchiveTrashList()
//        if(notes.count == 0){
//            notelistTV.isHidden = true
//
//            UserDefaults.standard.removeObject(forKey: "Archive")
//            Shared.sharedInstance.selectedIndex = 0
//            NotificationCenter.default.post(name: .didLoadNoteData, object: nil)
//
//            return
//        }
//
//        else{
//
//            SearchData=notes
//            notelistTV.isHidden = false
//
//        }
//
//        //        print("Notes Count:",notes.count, notes[0].title)
//        self.notelistTV.reloadData()
//    }
    
    func getTrashDataToDisplay(){
        notes = self.getTrashNoteList()
        
        if(notes.count == 0){
            notelistTV.isHidden = true
            UserDefaults.standard.removeObject(forKey: "Trash")
            
            Shared.sharedInstance.selectedIndex = 0
            NotificationCenter.default.post(name: .didLoadNoteData, object: nil)
            
            return
        }
            
        else{
            SearchData = notes
            notelistTV.isHidden = false
            
        }
        self.notelistTV.reloadData()
    }
    
    func getFolderDataToDisplay(){
        
        let categoryId = Shared.sharedInstance.categoryId
        print("categoryId:",categoryId)
        self.notes = dbHelper.getNotelistByNotebook(notebook_id: categoryId)
        if notes.count == 0{
                notelistTV.isHidden = true
                return
        }
        else{
            SearchData = notes
            notelistTV.isHidden = false
            self.notelistTV.reloadData()
                             
        }
           self.notelistTV.reloadData()
       }
    func getReminderDataToDisplay(){
        notes = self.getReminderNoteList()
        if(notes.count == 0){
            notelistTV.isHidden = true
            
            UserDefaults.standard.removeObject(forKey: "Reminder")
                       
            Shared.sharedInstance.selectedIndex = 0
            NotificationCenter.default.post(name: .didLoadNoteData, object: nil)
            
            return
        }
            
        else{
            
            SearchData=notes
            notelistTV.isHidden = false
            
        }
        
        //        print("Notes Count:",notes.count, notes[0].title)
        self.notelistTV.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearchTable.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didLoadNoteData, object: nil)
        self.addSideGesture(sender : self)
        actionButton.buttonColor = UIColor.appThemeColor()
        
        
        actionButton.addItem(title: "Text Note", image: UIImage(named: "text_note")) { item in
            self.actionButton.close()
            self.goToComposeNotebookPage(page: 0)
        }
        actionButton.addItem(title: "Handwriting", image: UIImage(named: "handwriting")) { item in
            // do something
            
            self.actionButton.close()
            self.goToComposeNotebookPage(page: 4)
            
            
            
            
        }
        
        actionButton.addItem(title: "Camera", image: UIImage(named: "camera")) { item in
            // do something
            self.actionButton.close()
            self.goToComposeNotebookPage(page: 2)
        }
        
        
        actionButton.addItem(title: "Attachment", image: UIImage(named: "attachement")) { item in
            // do something
            self.actionButton.close()
            self.goToComposeNotebookPage(page: 3)
        }
        
        
        actionButton.addItem(title: "Audio", image: UIImage(named: "Audio")) { item in
            // do something
            self.actionButton.close()
            self.goToComposeNotebookPage(page: 5)
        }
        actionButton.addItem(title: "Video", image: UIImage(named: "camera")) { item in
            // do something
            self.actionButton.close()
            self.goToComposeNotebookPage(page: 8)
        }
        
        //        actionButton.addItem(title: "Checklist", image: UIImage(named: "reminder")) { item in
        //            // do something
        //            self.actionButton.close()
        //            self.goToCheckListPage()
        //        }
        actionButton.addItem(title: "Reminder", image: UIImage(named: "reminder")) { item in
            // do something
            self.actionButton.close()
            self.goToComposeNotebookPage(page: 6)
        }
        actionButton.addItem(title: "checklist", image: UIImage(named: "attachement")) { item in
            // do something
            self.actionButton.close()
            // self.goToComposeNotebookPage(page: 6)
            self.showAlert(cancelTitle: nil, buttonTitles: ["OK"], title: Constant.AlertMessages.alertTitle, message: "Under Development")
            
        }
        view.addSubview(actionButton)
        
         getDataToDisplay()
        
        // Do any additional setup after loading the view.
        
    }
    
    @objc func onDidReceiveData(_ notification:Notification)
    {
        setDataonTable()
    }
    
    func goToComposeNotebookPage(page: Int) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ComposeNotesVC") as! ComposeNotesVC
        vc.delegate = self as ComposeNotesDelegate
        
        print(page)
        
        vc.flagForView = page
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func goToCheckListPage() {
        //        let story = UIStoryboard(name: "Main", bundle: nil)
        //        let vc = story.instantiateViewController(withIdentifier: "ChecklistVC") as! ChecklistVC
        //        //        vc.delegate = self as! ComposeNotesDelegate
        //        //        vc.flagForView = page
        //        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func goToComposeNotebookPage(page: Int, NotesObj: NoteModel) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ComposeNotesVC") as! ComposeNotesVC
        vc.delegate = self as ComposeNotesDelegate
        
        vc.isSelectFrom = self.lblHeader.text!
        
        vc.flagForView = page
        vc.notesData = NotesObj
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setDataonTable()
    {
            let selectedIndex = Shared.sharedInstance.selectedIndex
              print("SlectedIndex:",selectedIndex)
              if selectedIndex == 0 || selectedIndex == -1{
                  self.lblHeader.text = "All Notes"
                  
                  getDataToDisplay()
                  
                 // deleNoteFromid(note_id: "-9TJ3Bc_P61zFMwdtRnV_Qb")
                  
                  return
              }
              if selectedIndex == 1{
                  self.lblHeader.text = "Archive"
                  getArchiveDataToDisplay()
                  
                 // kkkk
                  return
              }
              if selectedIndex == 2{
                  self.lblHeader.text = "Trash"
                  getTrashDataToDisplay()
                  return
              }
              if selectedIndex == 3{
                  self.lblHeader.text = "Notebooks"
                  getFolderDataToDisplay()
                  return
              }
              if selectedIndex == 4{
                  self.lblHeader.text = "Group"
                  let groupId = Shared.sharedInstance.groupId
                  print("groupId:",groupId)
                  self.notes = dbHelper.getNotelistByGroup(group_id: groupId)
                  if notes.count == 0{
                      notelistTV.isHidden = true
                      return
                  }
                  else{
                      notelistTV.isHidden = false
                  }
                  self.notelistTV.reloadData()
              }
              if selectedIndex == 5{
                  self.lblHeader.text = "Kusuko notes"
              }
              if selectedIndex == 6{
                // self.present(cont!, animated: true, completion: nil)
              }
              
              if selectedIndex == 8{  //reminder
                   self.lblHeader.text = "Reminder"
                   getReminderDataToDisplay()
                   return
              }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        if UIDevice().userInterfaceIdiom == .phone {
            hgtHeader.constant = Globalfunc.getHeaderViewHeightForCurrentDevice()
        }
        setDataonTable()
    }
    
    
   
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getImage(timeStemp: String) -> UIImage?{
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("\(timeStemp)")
        if fileManager.fileExists(atPath: imagePAth){
            return UIImage(contentsOfFile: imagePAth)
        }else{
            print("No Image")
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        print("Number of notes: \(notes.count)")
        //return notes.count
        return SearchData.count
        //        return self.notesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = notelistTV.dequeueReusableCell(withIdentifier: "NoteListTableViewCell") as? NoteListTableViewCell else {
            return UITableViewCell()
        }
        
        
        let notesObj = self.SearchData[indexPath.row]
        
        cell.noteTitle.text = notesObj.title?.capitalized
        cell.noteDesc.text = notesObj.content?.html2String
        
        let strCreateTime = KUtils.convertDateToMMDDA(dateString: notesObj.dateCreate!)
        //let time = KUtils.getDateFromTimeStamp(timeStamp: Double("\(notesObj.creation)")! )
        cell.creationDate.text = strCreateTime
        let categoryId = notesObj.category_id
        
        
        if categoryId == "" || categoryId?.count == 0{
            cell.notebookColor.backgroundColor = UIColor.white
        }
        else{
            if UserDefaults.standard.value(forKey: "Folder")as? String == "Folder"{
                cell.notebookColor.backgroundColor = UIColor(hexString: "#\(notesObj.color_note!)")
            }
            else{
                cell.notebookColor.backgroundColor = UIColor.white
            }
        }
       
        if notesObj.alarm == "" || notesObj.alarm == nil{
            cell.alarmview.isHidden = true
        }
        else{
            cell.alarmview.isHidden = false
            let time = KUtils.convertDateToMMDDA(dateString: notesObj.alarm!)
            cell.alarmLbl.text = time
        }
        if notesObj.attachments.count > 0 {
            print("BukkedKey:", notesObj.attachments[0].bucket_key ?? "not found bukket key")
            print("MimeType:", notesObj.attachments.first?.mime_type ?? "Not found Mimetype")
            cell.imgNotes.isHidden = false
            let bukketKey = notesObj.attachments[0].bucket_key!
            if bukketKey != ""{
                let url = Constant.ImagePath + notesObj.attachments[0].bucket_key!
                let urlString = URL(string: url)
                
               // print(urlString)
                let mime = notesObj.attachments.first?.mime_type ?? "Not found Mimetype"
                if mime == "video/mp4" {
                    cell.imgNotes.image = #imageLiteral(resourceName: "video-player.png")
                }
                if mime == "audio/amr" {
                    cell.imgNotes.image = #imageLiteral(resourceName: "music-file.png")
                }
                if mime == "image/jpg" {
                   // cell.imgNotes.downloaded(from: urlString!, contentMode: .scaleAspectFit)
                    cell.imgNotes.sd_setImage(with: urlString!, placeholderImage: #imageLiteral(resourceName: "music-file.png"))
                }
            }
        }
        else{
            cell.imgNotes.isHidden = true
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let obj = self.notes[indexPath.row]
        let obj = self.SearchData[indexPath.row]
        if obj.attachments.count == 0{
            self.goToComposeNotebookPage(page: 10, NotesObj: obj)
            
        }else{
            let mime = obj.attachments.first?.mime_type ?? "Not found Mimetype"
            print("MimeType:", obj.attachments.first?.mime_type ?? "Not found Mimetype")
            if mime == "video/mp4" {
                self.goToComposeNotebookPage(page: 11, NotesObj: obj)
            }
            if mime == "audio/amr" {
                self.goToComposeNotebookPage(page: 12, NotesObj: obj)
            }
            if mime == "image/jpg" {
                self.goToComposeNotebookPage(page: 13, NotesObj: obj)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if self.lblHeader.text == "Trash"{

        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
             // delete item at indexPath
             print("Delete")
    let obj = self.SearchData[indexPath.row]

    if self.lblHeader.text == "Trash"{
        let refreshAlert = UIAlertController(title: "Trash", message: "Do You Want to Delete Note", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            //            let obj = self.notes[indexPath.row]
            
            self.delNoteFromDb(note: obj, note_id: obj.id!)
            self.getTrashDataToDisplay()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        self.present(refreshAlert, animated: true, completion: nil)
 
    }else if self.lblHeader.text == "All Notes"{
                     self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                     self.getDataToDisplay()
                            
                     UserDefaults.standard.set("Trash", forKey: "Trash")
                     UserDefaults.standard.synchronize()
             }else if self.lblHeader.text == "Reminder"{
                 
                 self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                 self.getReminderDataToDisplay()
                 
                 UserDefaults.standard.set("Trash", forKey: "Trash")
                 UserDefaults.standard.synchronize()
                 
                }else if self.lblHeader.text == "Archive"{
                 
                 self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                 self.getArchiveDataToDisplay()
                 
                 UserDefaults.standard.set("Trash", forKey: "Trash")
                 UserDefaults.standard.synchronize()
             }
             
             
         }
         
         let archive = UITableViewRowAction(style: .normal, title: "Archive") { (action, indexPath) in
             // Archive item at indexPath
             print("Archive")
 //            let obj = self.notes[indexPath.row]
             let obj = self.SearchData[indexPath.row]
             
             if self.lblHeader.text == "Archive"{
                 
                 //put back
                 self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: false)
                 self.getArchiveDataToDisplay()
                 
                 UserDefaults.standard.set("Archive", forKey: "Archive")
                 UserDefaults.standard.synchronize()
                 
                 
                 
             }
             
             else if self.lblHeader.text == "All Notes"{
                 
                 self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                 UserDefaults.standard.set("Archive", forKey: "Archive")
                 UserDefaults.standard.synchronize()
                 self.getDataToDisplay()
                 
             }
                 
             else if self.lblHeader.text == "Trash"{
                 
                 self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                 self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: false)
                 
                 self.getTrashDataToDisplay()
                 
                 UserDefaults.standard.set("Archive", forKey: "Archive")
                 UserDefaults.standard.synchronize()
                 
             }
             
             else if self.lblHeader.text == "Reminder"{
                 
                 self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                 self.getReminderDataToDisplay()
                 
                UserDefaults.standard.set("Archive", forKey: "Archive")
                UserDefaults.standard.synchronize()
             }

             
             
         }
         return [delete, archive]

            
        }else if self.lblHeader.text == "Archive"{
            let delete = UITableViewRowAction(style: .destructive, title: "Trash") { (action, indexPath) in
                        // delete item at indexPath
                        print("Trash")
            //            let obj = self.notes[indexPath.row]
                        let obj = self.SearchData[indexPath.row]
                        
                        //let selectedIndex = Shared.sharedInstance.selectedIndex
                        
                        if self.lblHeader.text == "Trash"{
                                self.delNoteFromDb(note: obj, note_id: obj.id!)
                                self.getTrashDataToDisplay()
                        }
                        else if self.lblHeader.text == "All Notes"{
                                self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                                self.getDataToDisplay()
                                       
                                UserDefaults.standard.set("Trash", forKey: "Trash")
                                UserDefaults.standard.synchronize()
                        }
                        
                        else if self.lblHeader.text == "Reminder"{
                            
                            self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                            self.getReminderDataToDisplay()
                            
                            UserDefaults.standard.set("Trash", forKey: "Trash")
                            UserDefaults.standard.synchronize()
                            
                        }
                        
                        else if self.lblHeader.text == "Archive"{
                            
                            self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                            self.getArchiveDataToDisplay()
                            
                            UserDefaults.standard.set("Trash", forKey: "Trash")
                            UserDefaults.standard.synchronize()
                        }
                        
                        
                    }
                    
                    let restores = UITableViewRowAction(style: .normal, title: "Restore ") { (action, indexPath) in
                        // Archive item at indexPath
                        print("Restore")
            //            let obj = self.notes[indexPath.row]
                        let obj = self.SearchData[indexPath.row]
                        
                        if self.lblHeader.text == "Archive"{
                            
                            //put back
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: false)
                            self.getArchiveDataToDisplay()
                            
                            UserDefaults.standard.set("Archive", forKey: "Archive")
                            UserDefaults.standard.synchronize()
                            
                            
                            
                        }
                        
                        else if self.lblHeader.text == "All Notes"{
                            
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                            UserDefaults.standard.set("Archive", forKey: "Archive")
                            UserDefaults.standard.synchronize()
                            self.getDataToDisplay()
                            
                        }
                            
                        else if self.lblHeader.text == "Trash"{
                            
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                            self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: false)
                            
                            self.getTrashDataToDisplay()
                            
                            UserDefaults.standard.set("Archive", forKey: "Archive")
                            UserDefaults.standard.synchronize()
                            
                        }
                        
                        else if self.lblHeader.text == "Reminder"{
                            
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                            self.getReminderDataToDisplay()
                            
                           UserDefaults.standard.set("Archive", forKey: "Archive")
                           UserDefaults.standard.synchronize()
                        }

                        
                        
                    }
                    return [delete, restores]

        }else{
            let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
                        // delete item at indexPath
                        print("Delete")
            //            let obj = self.notes[indexPath.row]
                        let obj = self.SearchData[indexPath.row]
                        
                        //let selectedIndex = Shared.sharedInstance.selectedIndex
                        
                        if self.lblHeader.text == "Trash"{
                                self.delNoteFromDb(note: obj, note_id: obj.id!)
                                self.getTrashDataToDisplay()
                        }
                        else if self.lblHeader.text == "All Notes"{
                                self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                                self.getDataToDisplay()
                                       
                                UserDefaults.standard.set("Trash", forKey: "Trash")
                                UserDefaults.standard.synchronize()
                        }
                        
                        else if self.lblHeader.text == "Reminder"{
                            
                            self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                            self.getReminderDataToDisplay()
                            
                            UserDefaults.standard.set("Trash", forKey: "Trash")
                            UserDefaults.standard.synchronize()
                            
                        }
                        
                        else if self.lblHeader.text == "Archive"{
                            
                            self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                            self.getArchiveDataToDisplay()
                            
                            UserDefaults.standard.set("Trash", forKey: "Trash")
                            UserDefaults.standard.synchronize()
                        }
                        
                        else if self.lblHeader.text == "Notebooks"{
                            
                            print(obj.id!)
                            self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: true)
                            self.getFolderDataToDisplay()

                            UserDefaults.standard.set("Folder", forKey: "Folder")
                            UserDefaults.standard.synchronize()
                            
                            UserDefaults.standard.set("Trash", forKey: "Trash")
                            UserDefaults.standard.synchronize()

                }
                        
                        
                    }
                    
                    let archive = UITableViewRowAction(style: .normal, title: "Archive") { (action, indexPath) in
                        // Archive item at indexPath
                        print("Archive")
            //            let obj = self.notes[indexPath.row]
                        let obj = self.SearchData[indexPath.row]
                        
                        if self.lblHeader.text == "Archive"{
                            
                            //put back
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: false)
                            self.getArchiveDataToDisplay()
                            
                            UserDefaults.standard.set("Archive", forKey: "Archive")
                            UserDefaults.standard.synchronize()
                        }
                        
                        else if self.lblHeader.text == "All Notes"{
                            
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                            UserDefaults.standard.set("Archive", forKey: "Archive")
                            UserDefaults.standard.synchronize()
                            self.getDataToDisplay()
                            
                        }
                            
                        else if self.lblHeader.text == "Trash"{
                            
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                            self.dbHelper.changeTrashNoteStatus(note_id: obj.id!, status: false)
                            
                            self.getTrashDataToDisplay()
                            
                            UserDefaults.standard.set("Archive", forKey: "Archive")
                            UserDefaults.standard.synchronize()
                            
                        }
                        
                        else if self.lblHeader.text == "Reminder"{
                            
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                            self.getReminderDataToDisplay()
                            
                           UserDefaults.standard.set("Archive", forKey: "Archive")
                           UserDefaults.standard.synchronize()
                            
                        }else if self.lblHeader.text == "Notebooks"{
                        
                            self.dbHelper.changeArchiveNoteStatus(note_id: obj.id!, status: true)
                                    self.getFolderDataToDisplay()

                                UserDefaults.standard.set("Folder", forKey: "Folder")
                                UserDefaults.standard.synchronize()
                            
                                UserDefaults.standard.set("Archive", forKey: "Archive")
                                UserDefaults.standard.synchronize()

                        }

                        
                        
                    }
                    return [delete, archive]

        }
    }
    
    func getStringDate(date: NSNumber) -> String{
        let ms = Date(timeIntervalSince1970: TimeInterval(truncating: date) / 1000)
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = DateFormatter.Style.short
        dateformatter.timeStyle = DateFormatter.Style.short
        return dateformatter.string(from: (ms))
        
    }
    
    func prepareNoteToSave(){
        //        self.prepareTextNote()
        //        self.prepareGroup()
        //        self.prepareNoteBook()
        //        self.prepareAttachment()
        
        self.prepareNoteWithAttachment()
    }
    
    func prepareNoteWithAttachment(){
        let date = Date.currentTimeStamp
        let mime = "image/jpg"
        let alarm = String(date)
        let owner = "10"
        let content = "<p data-tag=\"input\">See Attachment Status</p>"
        let uri = "http://dealntrip.com/wp-content/uploads/2016/09/a-1.jpg"
        
        
        let note = NoteModel(id: "", address: "", alarm: alarm, archived: false, checklist: false, content: content, creation: date, last_modification: date, group_id: "", latitude: 0, locked: false, longitude: 0, owner: owner, recurrence_rule: "", reminder_fired: false, title: "Note with attachments", trashed: false, category_id: "",dateCreate: KUtils.getCurrentDate(), userId: UserDefaults.standard.string(forKey: String(Constant.DefaultStorage.USER_ID))!,color_note: "#000000")
        
        let attach = AttachmentModel(attachment_id: NSNumber(value: date), uri: uri, name: "", bucket_key: "", size: 0, length: 0, mime_type: mime, sync: false, note_id: "", owner: owner)
        
        var attachments = [AttachmentModel]()
        attachments.append(attach)
        saveNoteWithAttachment(note: note, attachments:attachments)
        
        
    }
    
    func prepareGroup(){
        let date = Date().timeIntervalSince1970*1000
        let share_channel = "channel:group"
        let owner = "10"
        let group = GroupModel(id: "", color: "-1234567", desc: "", group_id: NSNumber(value: date), group_shared_channel: share_channel, members: ["testa@a.com", "di@a.com"], name: "My Group", owner: owner, pending_members: ["ram@sham.com"])
        //        saveGroupToDB(group: group)
    }
    
    func prepareTextNote(){
        let date = Date.currentTimeStamp
        
        let alarm = String(date)
        let owner = "10"
        let content = "<p data-tag=\"input\">Admin Status</p>"
        let note = NoteModel(id: "", address: "", alarm: alarm, archived: false, checklist: false, content: content, creation: date, last_modification: date, group_id: "", latitude: 0, locked: false, longitude: 0, owner: owner, recurrence_rule: "", reminder_fired: false, title: "First Note via", trashed: false, category_id: "",dateCreate: KUtils.getCurrentDate(),userId: UserDefaults.standard.string(forKey: String(Constant.DefaultStorage.USER_ID))!,color_note: "#000000")
        saveNoteToDB(note: note)
        
    }
    
    
    
    
}
//MARK:- Button Action
@available(iOS 11.0, *)
@available(iOS 11.0, *)
@available(iOS 11.0, *)
extension HomeVC{
    @IBAction func btnLeftMenuTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if lblHeader.text == "All Notes"{
            Shared.sharedInstance.IsFromAllNotes = true
        }
        self.frostedViewController.presentMenuViewController()
    }
    @IBAction func btnSearchAction(_ sender: Any) {
        self.view.endEditing(true)
        viewSearch.isHidden = false
    }
    @IBAction func btnBackSearchAction(_ sender: Any) {
        self.view.endEditing(true)
        viewSearch.isHidden = true
    }
    @available(iOS 11.0, *)
    @IBAction func btnSortListAction(_ sender: Any) {
        self.view.endEditing(true)

        var alertController = UIAlertController()
        if DeviceType.IS_IPAD{
            alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        }else{
            alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        }
        
        
        let firstAction = UIAlertAction(title:"Title", style: UIAlertActionStyle.default) {
            UIAlertAction in
            let selectedAlmanacEntries = self.notes.sorted {
                var isSorted = false
                if let first = $0.title, let second = $1.title {
                    isSorted = first < second
                }
                return isSorted
            }
            print(selectedAlmanacEntries)
            self.SearchData = selectedAlmanacEntries
            self.notelistTV.reloadData()
        }
        
        let secondAction = UIAlertAction(title: "Creation Date", style: UIAlertActionStyle.default) {
            UIAlertAction in

            let selectedAlmanacEntries = self.notes.sorted {
                var isSorted = false
                if let first = $0.dateCreate, let second = $1.dateCreate {
                    isSorted = first > second
                }
                return isSorted
            }
            print(selectedAlmanacEntries)
            self.SearchData = selectedAlmanacEntries
            self.notelistTV.reloadData()
        }
        
        let thirdAction = UIAlertAction(title: "Last Modification Date", style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
//            let selectedAlmanacEntries = self.notes.sorted {
//                           var isSorted = false
//                           if let first = $0.dateCreate, let second = $1.dateCreate {
//                               isSorted = first > second
//                           }
//                           return isSorted
//                       }
//                       print(selectedAlmanacEntries)
//                       self.SearchData = selectedAlmanacEntries
//                      self.notelistTV.reloadData()
        }
        
        let forthAction = UIAlertAction(title: "Reminder Date", style: UIAlertActionStyle.default) {
            UIAlertAction in
            let selectedAlmanacEntries = self.notes.sorted {
                           var isSorted = false
                           if let first = $0.alarm, let second = $1.alarm {
                               isSorted = first > second
                           }
                           return isSorted
                       }
                       print(selectedAlmanacEntries)
                       self.SearchData = selectedAlmanacEntries
                       self.notelistTV.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.setValue(attributedText(text: ""), forKey: "attributedTitle")
        alertController.addAction(firstAction)
        alertController.addAction(secondAction)
        alertController.addAction(thirdAction)
        alertController.addAction(forthAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnViewOptionsPressed(_ sender: Any) {
        self.view.endEditing(true)

        var alertController = UIAlertController()
        if DeviceType.IS_IPAD{
            alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        }else{
            alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        }
        
        
        let firstAction = UIAlertAction(title:"View Options", style: UIAlertActionStyle.default) {
            UIAlertAction in
            //    self.prepareNoteToSave()
            alertController.dismiss(animated: true, completion: nil)
            
        }
        
        let secondAction = UIAlertAction(title: "Sync", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.syncToDataBase()
            alertController.dismiss(animated: true, completion: nil)
            
        }
        
        let thirdAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            Shared.sharedInstance.selectedIndex = 5
            let cont = self.storyboard?.instantiateViewController(type: SettingVC.self)!
            self.present(cont!, animated: true, completion: nil)
            
        }
        
        //        let forthAction = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default) {
        //            UIAlertAction in
        //            self.logout()
        //            alertController.dismiss(animated: true, completion: nil)
        //        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.setValue(attributedText(text: ""), forKey: "attributedTitle")
        alertController.addAction(firstAction)
        alertController.addAction(secondAction)
        alertController.addAction(thirdAction)
        // alertController.addAction(forthAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                //                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}


