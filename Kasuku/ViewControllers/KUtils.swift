import UIKit

class KUtils: NSObject {
    
    //    class func showOKAlert(withTitle: String, message: String, from: UIViewController)  {
    //            let alert = UIAlertController(title: withTitle, message: message, preferredStyle: .alert)
    //            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
    //            }))
    //            from.present(alert, animated: true, completion: nil)
    //
    //    }
    class func showOKAlert(withTitle: String?, message: String?)  {
        let aler = UIAlertView.init(title: withTitle, message: message, delegate: nil, cancelButtonTitle: "Ok")
        aler.show()
    }
    class func getCurrentDate() -> String  {
       // let formatter = DateFormatter()
      //  formatter.dateFormat = "dd-MM-yyyy"
      //  return formatter.string(from: Date())
        
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        let result = formatter.string(from: date)
        return result
        
    }
    class func getCurrentDateYYYYMMDD() -> String  {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: Date())
    }
    class func convertDateToDDMMYYY(dateString: String) -> String  {
        if dateString != ""{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            if let date =  formatter.date(from: dateString){
                let formatter1 = DateFormatter()
                formatter1.dateFormat = "dd-MM-yyyy"
                return formatter1.string(from: date)
            }
            else{
                return ""
            }
        }
        return ""
    }
    
    class func convertDateToServer(dateString: String) -> String  {
        if dateString != ""{
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            if let date =  formatter.date(from: dateString) {
                let formatter1 = DateFormatter()
                formatter1.dateFormat = "yyyy-MM-dd"
                return formatter1.string(from: date)
            }
            return ""
        }
        return ""
    }
    
    class func convertDateToMMDDA(dateString: String) -> String  {
        if dateString != ""{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy HH:mm"
            if let date =  formatter.date(from: dateString){
                let formatter1 = DateFormatter()
                formatter1.dateFormat = "MMM dd, hh:mm a"
                return formatter1.string(from: date)
            }
            else{
                return ""
            }
        }
        return ""
    }
    
    
    class func getDateFromTimeStamp(timeStamp : Double) -> String {
        
//        
//        let dayTimePeriodFormatter = DateFormatter()
//        dayTimePeriodFormatter.dateFormat = "MMM dd, hh:mm a"
//        // UnComment below to get only time
//        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
//        
//        let dateString = dayTimePeriodFormatter.string(from: date as Date)
//        return dateString
        
        
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone =  .current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
}
