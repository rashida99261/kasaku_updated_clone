import UIKit
import JJFloatingActionButton

class NotesbooksVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnFloat: UIView!
    let actionButton = JJFloatingActionButton()
    
    @IBAction func btnLeftMenuTapped(_ sender: UIButton) {
        self.frostedViewController.presentMenuViewController()
    }
    
    /**
     *
     *  Method  for configuring pan gesture.
     *
     */
    @objc func panGestureRecognized(sender : UIPanGestureRecognizer){
        self.frostedViewController.panGestureRecognized(sender)
    }
    
    
    /**
     *
     *  Method for adding UIScreenEdgePanGestureRecorgnizer to Side Menu.
     *
     * @param sender UIViewController
     *
     */
    func addSideGesture(sender : UIViewController){
        let panGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.panGestureRecognized(sender:)))
        panGesture.edges = .left
        sender.frostedViewController.limitMenuViewSize = true
        sender.frostedViewController.menuViewSize = CGSize(width:Constant.ScreenDimension.width * 0.78,height:Constant.ScreenDimension.height)
        sender.view.addGestureRecognizer(panGesture)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        actionButton.frame = self.btnFloat.frame
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSideGesture(sender : self)
        actionButton.buttonColor = UIColor.appThemeColor()
        
        
        actionButton.addItem(title: "Text Note", image: UIImage(named: "text_note")) { item in
            self.actionButton.close()
            let cont = self.storyboard?.instantiateViewController(type: TextNoteVC.self)!
            self.navigationController?.pushViewController(cont!, animated: true)
        }
        actionButton.addItem(title: "Handwriting", image: UIImage(named: "handwriting")) { item in
            // do something
            
            self.actionButton.close()
            let cont = self.storyboard?.instantiateViewController(type: HandwritingVC.self)!
            self.navigationController?.pushViewController(cont!, animated: true)
        }
        
        actionButton.addItem(title: "Camera", image: UIImage(named: "camera")) { item in
            // do something
            self.actionButton.close()
            let cont = self.storyboard?.instantiateViewController(type: TextNoteVC.self)!
            self.navigationController?.pushViewController(cont!, animated: true)
        }
        
        
        actionButton.addItem(title: "Attachment", image: UIImage(named: "attachement")) { item in
            // do something
            self.actionButton.close()
            let cont = self.storyboard?.instantiateViewController(type: TextNoteVC.self)!
            self.navigationController?.pushViewController(cont!, animated: true)
        }
        
        
        actionButton.addItem(title: "Audio", image: UIImage(named: "Audio")) { item in
            // do something
            self.actionButton.close()
            let cont = self.storyboard?.instantiateViewController(type: TextNoteVC.self)!
            self.navigationController?.pushViewController(cont!, animated: true)
        }
        
        
        actionButton.addItem(title: "Reminder", image: UIImage(named: "reminder")) { item in
            // do something
            self.actionButton.close()
            let cont = self.storyboard?.instantiateViewController(type: TextNoteVC.self)!
            self.navigationController?.pushViewController(cont!, animated: true)
        }
        
        view.addSubview(actionButton)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
    }
}

extension NotesbooksVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(type: NotebooksTableCell.self)!
        return cell
    }
}
extension NotesbooksVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
