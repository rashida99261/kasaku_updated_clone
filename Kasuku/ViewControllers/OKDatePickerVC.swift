import UIKit
protocol OKDateDelegate{
    func selecteDate(date: String, dateServer:String?, flag: Int)
}
class OKDatePickerVC: UIViewController {
var delegate: OKDateDelegate!
    @IBOutlet weak var mDatePicker: UIDatePicker!
    var flag : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mDatePicker.minimumDate = Date()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
       let dateString = self.getDateString()
        let serverDateString = self.getServerDateString()
        self.delegate.selecteDate(date: dateString, dateServer: serverDateString, flag: flag)
    }
    func getDateString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        return formatter.string(from: mDatePicker.date)
    }
    func getServerDateString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:MM"
        return formatter.string(from: mDatePicker.date)
    }
}
